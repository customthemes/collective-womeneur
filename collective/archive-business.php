<?php
/**
 * This file adds the custom business post type archive template to the Hello Theme.
 *
 * @author Riot Customs LLC
 * @package Generate
 * @subpackage Customizations
 */

//* Remove the breadcrumb navigation
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove the post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );

//* Remove the post image
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

//* Add business body class to the head
add_filter( 'body_class', 'collective_add_business_body_class' );
function collective_add_business_body_class( $classes ) {

	$classes[] = 'business';
	return $classes;

}

//* Add featured image after post title
add_action( 'genesis_entry_header', 'collective_business_grid', 9 );
function collective_business_grid() {
	if ( $image = genesis_get_image( 'format=url&size=business' ) ) {
		printf( '<div class="business-featured-image"><a href="%s" rel="bookmark"><img src="%s" alt="%s" /></a></div>', get_permalink(), $image, the_title_attribute( 'echo=0' ) );
	}
}

// Force excerpts regardless of theme's Content Archive settings
add_filter( 'genesis_pre_get_option_content_archive', 'sk_force_excerpts' );
function sk_force_excerpts() {
	return 'excerpts';
}

//* Add Excerpt support to manual excerpts
function manual_excerpt_more( $excerpt ) {
	$excerpt_more = '';
	if( has_excerpt() ) {
    	$excerpt_more = '<div class="more-button"><a class="more-link" href="' . get_permalink() . '" rel="nofollow">Start Here</a>';
	}
	return $excerpt . $excerpt_more;
}
add_filter( 'get_the_excerpt', 'manual_excerpt_more' );

//* Modify the WordPress read more link
add_filter( 'get_the_content_more_link', 'collective_more_link' );
function collective_more_link() {
    return '
            <div class="more-button"><a class="more-link" href="' . get_permalink() . '">' . __('Start Here', 'collective') . '</a>
            </div>';
}

function collective_custom_excerpt_more( $more ) {
    return '
            <div class="more-button"><a class="more-link" href="' . get_permalink() . '">' . __('Start Here', 'collective') . '</a>
            <span class="meta-divider"></span></div>';
}
add_filter( 'excerpt_more', 'collective_custom_excerpt_more' );


//* Remove the post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Reposition Post Navigation (Previous / Next or Numeric)
remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );
add_action( 'genesis_after_content', 'genesis_posts_nav' );


genesis();
