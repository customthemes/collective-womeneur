<?php

/**
 * This file adds the members directory template to the Ariana Theme.
 *
 * @author Riot Customs
 * @package Generate
 * @subpackage Customizations
 */

/*
Template Name: Members Directory
*/

// Add custom body class to the head
add_filter( 'body_class', 'directory_add_body_class' );
function directory_add_body_class( $classes ) {
   $classes[] = 'directory-page';
   return $classes;
}

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove site footer widgets
remove_action( 'genesis_after', 'genesis_footer_widget_areas' );


//* Run the Genesis loop
genesis();