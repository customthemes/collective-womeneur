<?php
/**
 * Womeneur - Collective.
 *
 * This file adds functions to the Womeneur Theme.
 *
 * @package Womeneur
 * @author  Riot Customs LLC
 * @license GPL-2.0+
 * @link    http://www.riotcustoms.com/
 */

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'womeneur', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'womeneur' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Womeneur' );
define( 'CHILD_THEME_URL', 'http://www.riotcustoms.com/' );
define( 'CHILD_THEME_VERSION', '2.2.3' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'womeneur_enqueue_scripts_styles' );
function womeneur_enqueue_scripts_styles() {
	wp_enqueue_style( 'womeneur-fonts', '//fonts.googleapis.com/css?family=Dosis|Open+Sans', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'womeneur-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );
	$output = array(
		'mainMenu' => __( 'Menu', 'womeneur' ),
		'subMenu'  => __( 'Menu', 'womeneur' ),
	);
	wp_localize_script( 'womeneur-responsive-menu', 'genesisSampleL10n', $output );
	wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	add_action( 'wp_enqueue_scripts', 'collective_smooth_scroll' );
function collective_smooth_scroll() {

	wp_enqueue_script( 'scrollTo', get_stylesheet_directory_uri() . '/js/jquery.scrollTo.min.js', array( 'jquery' ), '1.4.5-beta', true );
	wp_enqueue_script( 'localScroll', get_stylesheet_directory_uri() . '/js/jquery.localScroll.min.js', array( 'scrollTo' ), '1.2.8b', true );
	wp_enqueue_script( 'scrollto-init', get_stylesheet_directory_uri() . '/js/scrollto-init.js', array( 'localScroll' ), '', true );

}

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'collectiveip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'width'           => 140,
	'height'          => 140,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Remove the header right widget area
unregister_sidebar( 'header-right' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 0 );

//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_before_header', 'genesis_do_subnav' );

//* Unregister secondary sidebar
unregister_sidebar( 'sidebar-alt' );

//* Reduce the secondary navigation menu to one level depth
add_filter( 'wp_nav_menu_args', 'womeneur_secondary_menu_args' );
function womeneur_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}
// Custom Credits
add_filter('genesis_footer_creds_text', 'riot_footer_creds_filter');
function riot_footer_creds_filter( $creds ) {
	$creds = '[footer_copyright] &middot; <a href="https://www.womeneur.com/">Womeneur™</a> &middot; All Rights Reserved';
	return $creds;
}

//* Add WooComm themes support
add_theme_support( 'genesis-connect-woocommerce');

//* Create a custom Gravatar
add_filter( 'avatar_defaults', 'riot_custom_gravatar' );
function riot_custom_gravatar ($avatar) {
	$custom_avatar = get_stylesheet_directory_uri() . '/images/gravatar.png';
	$avatar[$custom_avatar] = "Womeneur Gravatar";
	return $avatar;
}

//* Create Business Kit Type custom taxonomy
add_action( 'init', 'collective_type_taxonomy' );
function collective_type_taxonomy() {

	register_taxonomy( 'business-type', 'business',
		array(
			'labels' => array(
				'name'          => _x( 'Types', 'taxonomy general name', 'collective' ),
				'add_new_item'  => __( 'Add New Business Kit Type', 'collective' ),
				'new_item_name' => __( 'New Business Kit Type', 'collective' ),
			),
			'exclude_from_search' => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array( 'slug' => 'business-type', 'with_front' => false ),
			'show_ui'             => true,
			'show_tagcloud'       => false,
		)
	);

}

//* Create business custom post type
add_action( 'init', 'collective_business_post_type' );
function collective_business_post_type() {

	register_post_type( 'business',
		array(
			'labels' => array(
				'name'          => __( 'Business Kit', 'collective' ),
				'singular_name' => __( 'business-kit', 'collective' ),
			),
			'has_archive'  => true,
			'hierarchical' => true,
			'menu_icon'    => get_stylesheet_directory_uri() . '/images/icons/portfolio.png',
			'public'       => true,
			'rewrite'      => array( 'slug' => 'business', 'with_front' => false ),
			'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'revisions', 'sidebars', 'genesis-seo', 'genesis-cpt-archives-settings', 'widgets' ),
			'taxonomies'   => array( 'business-type' ),

		)
	);

}

//* Add Business Kit Type Taxonomy to columns
add_filter( 'manage_taxonomies_for_business_columns', 'collective_business_columns' );
function collective_business_columns( $taxonomies ) {

	$taxonomies[] = 'business-type';
	return $taxonomies;

}

//* Load Admin Stylesheet
add_action( 'admin_enqueue_scripts', 'riot_load_admin_styles' );
function riot_load_admin_styles() {

	wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );

}

//* Change the number of business items
add_action( 'pre_get_posts', 'collective_business_items' );
function collective_business_items( $query ) {

	if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'business' ) ) {
		$query->set( 'posts_per_page', '12' );
	}

}

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


// Register front-page widget areas.
for ( $i = 1; $i <= 7; $i++ ) {
	genesis_register_widget_area(
		array(
			'id'          => "front-page-{$i}",
			'name'        => __( "Front Page {$i}", 'womeneur' ),
			'description' => __( "This is the front page {$i} section.", 'womeneur' ),
		)
	);
}
genesis_register_sidebar( array(
	'id'		=> 'login-top',
	'name'		=> __( 'Login Top', 'riot' ),
	'description'	=> __( 'This is the widget area for the Login Top.', 'riot' ),
) );
genesis_register_sidebar( array(
	'id'		=> 'login-section-1',
	'name'		=> __( 'Login Left', 'riot' ),
	'description'	=> __( 'This is the widget area for the Login Left.', 'riot' ),
) );
genesis_register_sidebar( array(
	'id'		=> 'login-section-2',
	'name'		=> __( 'Login Right', 'riot' ),
	'description'	=> __( 'This is the widget area for the Login Right.', 'riot' ),
) );
//* Hook before content widget area for welcome page
genesis_register_sidebar( array(
	'id'		=> 'welcome-widget',
	'name'		=> __( 'Welcome Widget', 'riot' ),
	'description'	=> __( 'This is the widget area for the Welcome Widget.', 'riot' ),
) );
add_action( 'genesis_before_content', 'riot_top_ad_widget_area' );
function riot_top_ad_widget_area() {
	if ( is_page('64') )
	genesis_widget_area ('welcome-widget', array(
        'before' => '<div class="welcome-widget"><div class="wrap">',
        'after' => '</div></div>',
	) );
}

// Register new sidebar with ID ==> biz-sidebar
genesis_register_sidebar( array(
    'id' => 'biz-sidebar',
    'name' => 'Buisness Kits Sidebar',
    'description' => 'SideBar for Business Custom Post Type',
) );
add_action('get_header','riot_biz_sidebar');
function riot_biz_sidebar() {
    if ( is_singular('business') || is_post_type_archive('business')) { // Here "biz" is a slug name for my CPT
        remove_action( 'genesis_sidebar', 'genesis_do_sidebar' ); //remove the default genesis sidebar
        add_action( 'genesis_sidebar', 'riot_add_sidebar' ); //add an action hook to call the function for my custom sidebar
    }
}
 
//Function to output my custom sidebar
function riot_add_sidebar() {
    dynamic_sidebar( 'biz-sidebar' ); // id of sidebar which you just registered 
}


//* Register before content widget area
genesis_register_sidebar( array(
	'id'          => 'before-content',
	'name'        => __( 'Before Content', 'collective' ),
	'description' => __( 'This is the before content widget area.', 'collective' ),
) );
//* Hook before header widget area before content area
add_action( 'genesis_before_content', 'riot_before_content_widget_area' );
function riot_before_content_widget_area() {
	genesis_widget_area( 'before-content', array(
		'before' => '<div class="before-content widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );
}