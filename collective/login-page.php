<?php

/**
 * This file adds the Login template to the espresso theme.
 *
 * @author Riot Customs LLC
 * @package Generate
 * @subpackage Customizations
 */

/*
Template Name: Login Page
*/

// Add custom body class to the head
add_filter( 'body_class', 'riot_add_body_class' );
function riot_add_body_class( $classes ) {
   $classes[] = 'collective-login';
   return $classes;
}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove navigation
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_before', 'genesis_do_subnav' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove site footer widgets
remove_action( 'genesis_after', 'genesis_footer_widget_areas' );

//* Remove site footer elements
remove_action( 'genesis_after', 'genesis_footer_markup_open', 11 );
remove_action( 'genesis_after', 'genesis_do_footer', 12 );
remove_action( 'genesis_after', 'genesis_footer_markup_close', 13 );

//* Add cover page widgets
	add_action( 'genesis_loop', 'riot_coverpage_widgets' );

function riot_coverpage_widgets() {
  	if ( is_active_sidebar( 'login-top' )){
		echo '<div class="login-top">';
		genesis_widget_area( 'login-top', array(
			'before' => '<div class="login-top widget-area">',
			'after'  => '</div>',

	) );
}
  if ( is_active_sidebar( 'login-section' )) {
	
	echo '<div class="login-section">';
	genesis_widget_area( 'login-section', array(
		'before' => '<div class="login-section widget-area">',
		'after'  => '</div>',
	) );
	echo '</div>';
	
	}
	if ( is_active_sidebar( 'login-section-1' ) || is_active_sidebar('login-section-2')) {
		echo '<div class="login-section widget-area">';

		genesis_widget_area( 'login-section-1', array(
			'before' => '<div class="login-section-1 one-half first">',
			'after'  => '</div>',
		) );
		
		genesis_widget_area( 'login-section-2', array(
			'before' => '<div class="login-section-2 one-half">',
			'after'  => '</div>',
		) );

	}
}

genesis();
