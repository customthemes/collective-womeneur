<?php
/**
 * This file adds the custom portfolio post type single post template to the Hello Theme.
 
 * @author Riot Customs LLC
 * @package Generate
 * @subpackage Customizations
 */

 //* Force content-sidebar layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );
//* Remove the post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );
//* Remove the author box on single posts
remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );
//* Remove the post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

add_action( 'genesis_entry_content', 'sk_do_post_content', 9 );
function sk_do_post_content() {
	echo '<div class="entry-content">';
?>
<!-- check if the flexible content field has rows of data -->
	<?php if( have_rows('flexible_content') ): ?>

	     <!-- loop through the rows of data -->
	    <?php while ( have_rows('flexible_content') ) : the_row(); ?>

	    	<!-- button field -->
	        <?php if( get_row_layout() == 'button' ): ?>

	        	<a class="biz-button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a><br>

			<!-- content only -->
        	<?php elseif( get_row_layout() == 'content_only' ): ?>
							<p>
								<?php the_sub_field('content'); ?>
							</p>
		<!-- content with title -->
        	<?php elseif( get_row_layout() == 'content_title' ): ?>
        			<h2><?php 
								the_sub_field('title'); ?>
							</h2>
							<p>
								<?php the_sub_field('content'); ?>
							</p>

	        <?php endif; ?>
	    <?php endwhile; ?>
	<?php else : ?>

	    <!-- no layouts found -->

	<?php endif; ?>

<?php  ?>

<?php
	echo '</div>';
}
	
//* Previous and Next Post navigation
add_action('genesis_after_entry', 'sk_custom_post_nav');
function sk_custom_post_nav() {
	echo '<div class="prev-next-post-links">';
		previous_post_link('<div class="previous-post-link">&laquo; %link</div>', '<strong>%title</strong>' );
		next_post_link('<div class="next-post-link">%link &raquo;</div>', '<strong>%title</strong>' );
	echo '</div>';
}

//* Run the Genesis loop
genesis();